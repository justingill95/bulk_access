from sharedlibrary.anaplandal.src.anaplan_auth import AnaplanAuthClient
from sharedlibrary.anaplandal.src.anaplan_actions import AnaplanActionsClient


if __name__ == "__main__":
    model_id = "37EB84BF998941439F1AF4FEE8D2BF26"
    workspace_id = "8a81b0107522c7f301752dae5b2a425e"
    username = "justingillanaplanmail@gmail.com"
    password = "Coolio1212%"

    # Authenticate
    auth_client = AnaplanAuthClient(base_endpoint='https://auth.anaplan.com')
    token = auth_client.authenticate(username, password)

    # Actions Client connection
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")

    # Example call
    print(act_client.list_views(token["tokenValue"], workspace_id, model_id))

