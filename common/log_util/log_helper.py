import json
import os
from logging import LoggerAdapter
from typing import Dict, Optional

from common.logging.plogger import PLogger


class XcomAndEnvWrapper:
    """
    Wraps env variables and XcomData (if present)
    """

    def __init__(self, extra_env_vars):
        self.extra_env_vars = dict() if extra_env_vars is None else extra_env_vars
        self._xcom_data = self._init_xcom_data()

    def get_var(self, key) -> Optional[str]:
        """
        Returns env var with fallback to XcomData
        """
        val = self.extra_env_vars.get(key)
        if val is not None:
            return val

        val = os.environ.get(key)
        if val is not None:
            return val

        val = self._xcom_data.get(key)
        return val

    @staticmethod
    def _init_xcom_data() -> Dict:
        xcom_data = os.environ.get("XCOM_DATA")
        if xcom_data is None:
            return dict()

        xcom_data = json.loads(xcom_data)
        return xcom_data


def configure_logger_adapter(logger_name: str, extra_env_vars: Dict[str, str] = None) -> LoggerAdapter:
    """
    Configure logger adaptor from env variables with fallback to XCOM_DATA
    """
    xe_warp = XcomAndEnvWrapper(extra_env_vars)

    app_name = xe_warp.get_var("APP_NAME")
    env_type = xe_warp.get_var("ENV_TYPE")
    env_name = xe_warp.get_var("ENV_NAME")
    trace_id = xe_warp.get_var("TRACE_ID")
    trace_path = xe_warp.get_var("TRACE_PATH")
    flow_run_id = xe_warp.get_var("FLOW_RUN_ID")
    flow_name = xe_warp.get_var("FLOW_NAME")
    customer_guid = xe_warp.get_var("CUSTOMER_GUID")
    forecast_data_collection_id = xe_warp.get_var("FORECAST_DATA_COLLECTION_ID")

    ctx = dict()

    if app_name:
        ctx["app_name"] = app_name

    if env_type:
        ctx["env_type"] = env_type

    if env_name:
        ctx["env_name"] = env_name

    if trace_id:
        ctx["trace_id"] = trace_id

    if trace_path:
        ctx["trace_path"] = trace_path

    if flow_run_id:
        ctx["flow_run_id"] = flow_run_id

    if flow_name:
        ctx["flow_name"] = flow_name

    if customer_guid:
        ctx["customer_guid"] = customer_guid

    if forecast_data_collection_id:
        ctx["forecast_data_collection_id"] = forecast_data_collection_id

    logger_adapter = LoggerAdapter(
        PLogger(logger_name),
        ctx,
    )
    return logger_adapter
