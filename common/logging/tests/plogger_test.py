import json
import logging
import os

import pytest

from common.logging.plogger import PLogger


def get_info_log_as_dict(captured):
    return [json.loads(line) for line in captured.out.split("\n")[:-1]]


def get_info_log_as_string(captured):
    return captured.out.split("\n")[:-1][0]


def get_err_log_as_dict(captured):
    return [json.loads(line) for line in captured.err.split("\n")[:-1]]


@pytest.fixture(scope="function")
def set_debug(monkeypatch):
    monkeypatch.setenv("LOGLEVEL", "DEBUG")


@pytest.fixture(scope="function")
def set_invalid_level(monkeypatch):
    monkeypatch.setenv("LOGLEVEL", "FOOBAR")


@pytest.fixture(scope="function")
def set_json_format_str_true(monkeypatch):
    monkeypatch.setenv("JSON_FORMAT", "true")


@pytest.fixture(scope="function")
def set_json_format_str_upper_true(monkeypatch):
    monkeypatch.setenv("JSON_FORMAT", "TRUE")


@pytest.fixture(scope="function")
def set_json_format_str_false(monkeypatch):
    monkeypatch.setenv("JSON_FORMAT", "False")


class TestPLogger:
    def test_get_name(self):
        logger = PLogger("test-PLogger")
        assert logger.name == "test-PLogger"

    def test_info(self, capsys):
        logger = PLogger("test-PLogger", level="INFO")
        logger.info("info message")
        captured_logs = get_info_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1
        assert captured_logs[0].get("levelname") == "INFO"
        assert captured_logs[0].get("filename") is not None
        assert captured_logs[0].get("funcName") is not None
        assert captured_logs[0].get("lineno") is not None
        assert captured_logs[0].get("message") == "info message"

    def test_debug(self, capsys):
        logger = PLogger("test-PLogger", level="DEBUG")
        logger.debug("debug message")
        captured_logs = get_info_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1
        assert captured_logs[0].get("levelname") == "DEBUG"
        assert captured_logs[0].get("message") == "debug message"

    def test_warning(self, capsys):
        logger = PLogger("test-PLogger")
        logger.warning("warning message")
        captured_logs = get_err_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1
        assert captured_logs[0].get("levelname") == "WARNING"
        assert captured_logs[0].get("message") == "warning message"

    def test_error(self, capsys):
        logger = PLogger("test-PLogger")
        logger.error("error message")
        captured_logs = get_err_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1
        assert captured_logs[0].get("levelname") == "ERROR"
        assert captured_logs[0].get("message") == "error message"

    def test_exception(self, capsys):
        logger = PLogger("test-PLogger")
        try:
            raise ValueError("some error")
        except ValueError as e:
            logger.exception("exception message")

        captured_logs = get_err_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1
        assert captured_logs[0].get("levelname") == "ERROR"
        assert "exc_info" in captured_logs[0]
        assert captured_logs[0].get("message") == "exception message"

    def test_critical(self, capsys):
        logger = PLogger("test-PLogger")
        logger.critical("critical message")
        captured_logs = get_err_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1
        assert captured_logs[0].get("levelname") == "CRITICAL"
        assert captured_logs[0].get("message") == "critical message"

    def test_info_with_kwargs(self, capsys):
        logger = PLogger("test-PLogger", level="INFO")
        logger.info("info message", key1="value1")
        captured_logs = get_info_log_as_dict(capsys.readouterr())
        assert "key1" in captured_logs[0]
        assert captured_logs[0].get("key1") == "value1"

    def test_info_with_extra_arg(self, capsys):
        logger = PLogger("test-PLogger", level="INFO")
        logger.info("info message", extra={"key1": "value1"})
        captured_logs = get_info_log_as_dict(capsys.readouterr())
        assert "key1" in captured_logs[0]
        assert captured_logs[0].get("key1") == "value1"

    def test_plogger_make_record(self):
        logger = PLogger("test-PLogger", level="INFO")
        extra = {"key1": "value1"}
        record = logger.makeRecord(
            "test-PLogger", logging.INFO, "test_plogger_make_record", 98, "info message", None, None, extra=extra
        )
        assert record.levelname == "INFO"
        assert record.msg == "info message"
        assert record.key1 == "value1"

    def test_plogger_level_from_env_vars(self, capsys, set_debug):
        logger = PLogger("test-PLogger")
        logger.debug("debug message")
        captured_logs = get_info_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1
        assert captured_logs[0].get("levelname") == "DEBUG"
        assert captured_logs[0].get("message") == "debug message"

    def test_plogger_default_log_level(self, capsys):
        # default level is info, expect no logs to be written
        logger = PLogger("test-PLogger")
        logger.debug("debug message")
        captured_logs = get_info_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1

    def test_plogger_invalid_log_level(self, set_invalid_level):
        with pytest.raises(ValueError):
            PLogger("test-PLogger")

    def test_context_not_affected_by_additional_kwargs(self, capsys):
        logger = PLogger("test-PLogger", level="INFO")
        ctx = {"a": 1, "b": 2}
        logger.info("info message", extra=ctx, c=3)
        assert "c" not in ctx

    def test_json_formatter_str_true_debug(self, capsys, set_json_format_str_true):
        logger = PLogger("test-PLogger", level="DEBUG")
        logger.debug("debug message")
        captured_logs = get_info_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1
        assert captured_logs[0].get("levelname") == "DEBUG"
        assert captured_logs[0].get("message") == "debug message"

    def test_json_formatter_str_upper_true_debug(self, capsys, set_json_format_str_upper_true):
        logger = PLogger("test-PLogger", level="DEBUG")
        logger.debug("debug message")
        captured_logs = get_info_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1
        assert captured_logs[0].get("levelname") == "DEBUG"
        assert captured_logs[0].get("message") == "debug message"

    def test_json_formatter_str_false_debug(self, capsys, set_json_format_str_false):
        logger = PLogger("test-PLogger", level="DEBUG")
        logger.debug("debug message")
        captured_logs = get_info_log_as_string(capsys.readouterr())
        assert "debug message" in captured_logs

    def test_json_formatter_str_true_info(self, capsys, set_json_format_str_true):
        logger = PLogger("test-PLogger", level="INFO")
        logger.info("info message")
        captured_logs = get_info_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1
        assert captured_logs[0].get("levelname") == "INFO"
        assert captured_logs[0].get("message") == "info message"

    def test_json_formatter_str_upper_true_info(self, capsys, set_json_format_str_upper_true):
        logger = PLogger("test-PLogger", level="INFO")
        logger.info("info message")
        captured_logs = get_info_log_as_dict(capsys.readouterr())
        assert len(captured_logs) == 1
        assert captured_logs[0].get("levelname") == "INFO"
        assert captured_logs[0].get("message") == "info message"

    def test_json_formatter_str_false_info(self, capsys, set_json_format_str_false):
        logger = PLogger("test-PLogger", level="INFO")
        logger.info("info message")
        captured_logs = get_info_log_as_string(capsys.readouterr())
        assert "info message" in captured_logs
