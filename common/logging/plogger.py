import copy
import logging
import os
import sys
from datetime import datetime

from pythonjsonlogger import jsonlogger

from common.logging.filters import ErrorFilter, InfoFilter

_logger_instances = {}


class PLogger(logging.Logger):
    """PLogger extends logging.Logger and enhance it with pythonjsonlogger"""

    # Make sure than PLogger is a singleton by the logger name attribute
    def __new__(cls, *args, **kwargs):
        """Create new logger instance. We make sure there is only a single instance per name attribute"""
        name = kwargs["name"] if "name" in kwargs else args[0]
        for _clss, instance in _logger_instances.items():
            if instance.name == name:
                return instance

        instance = super().__new__(cls)
        _logger_instances[cls] = instance
        return instance

    def __init__(self, name, level=None):
        if not level:
            level = os.environ.get("LOGLEVEL", "DEBUG")
        if level not in logging._nameToLevel:
            raise ValueError(f"Invalid log level {level}, supported levels: {logging._nameToLevel.keys()}")

        self.json_format = os.environ.get("JSON_FORMAT", "True")
        log_level = getattr(logging, level)
        super().__init__(name, log_level)
        self._defineHandlers(name)

    def _defineHandlers(self, name):
        out_msg_prefix, err_msg_prefix = self._get_msg_prefixes(name)

        out_handler = logging.StreamHandler(sys.stdout)
        formatter = self._get_formatter_type(out_msg_prefix)
        out_handler.setFormatter(formatter)
        out_handler.addFilter(InfoFilter())

        err_formatter = self._get_formatter_type(err_msg_prefix)
        err_handler = logging.StreamHandler(sys.stderr)
        err_handler.setFormatter(err_formatter)
        err_handler.addFilter(ErrorFilter())

        self.addHandler(out_handler)
        self.addHandler(err_handler)

    def makeRecord(self, name, level, fn, lno, msg, args, exc_info, func=None, extra=None, sinfo=None):
        """
        A factory method which can be overridden in subclasses to create
        specialized LogRecords.
        """
        rv = super().makeRecord(name, level, fn, lno, msg, args, exc_info, func=func, extra=extra, sinfo=sinfo)

        #  Not sure these are required... just making it the same key names as in the Analog Python module
        rv.__dict__["timestamp"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
        rv.__dict__["name"] = name

        return rv

    def debug(self, msg, *args, **kwargs):
        """Debug level log"""
        self.log(logging.DEBUG, msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        """Info level log"""
        self.log(logging.INFO, msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        """Warning level log"""
        self.log(logging.WARNING, msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        """Error level log"""
        self.log(logging.ERROR, msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        """Critical level log"""
        self.log(logging.CRITICAL, msg, *args, **kwargs)

    def log(self, level, msg, *args, **kwargs):
        """Log a message"""
        if self.isEnabledFor(level):
            # Don't want these to get into the 'extra' dict
            exc_info = kwargs.pop("exc_info", None)
            stack_info = kwargs.pop("stack_info", False)
            # default stackLevel is set to 1, which will point to this file's debug/info/... functions, we want two calls
            # up in the hierarchy, in order to print the actual call to the log
            stacklevel = 3

            # This is in order to support any kwargs to be passed to the log action on top of the 'extra' dict.
            # We have to copy the extra argument, otherwise, if we have any other kwargs, they will be added to the
            # original dict that represents extra.
            extra = copy.deepcopy(kwargs.pop("extra", {}))
            if kwargs:
                extra.update(kwargs)

            super().log(level, msg, *args, exc_info=exc_info, extra=extra, stack_info=stack_info, stacklevel=stacklevel)

    def _get_formatter_type(self, out_msg_prefix):
        if self.json_format.lower() == "true":
            return jsonlogger.JsonFormatter(out_msg_prefix)
        return logging.Formatter(out_msg_prefix)

    def _get_msg_prefixes(self, name):
        base_fmt = "%(name)s %(levelname)s %(timestamp)s %(filename)s %(funcName)s %(lineno)d %(message)s"
        out_msg_prefix = f"analog-{name} {base_fmt}"
        err_msg_prefix = f"analog-err-{name} {base_fmt}"

        return out_msg_prefix, err_msg_prefix
