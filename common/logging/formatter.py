import os
from datetime import datetime

from pythonjsonlogger import jsonlogger

APP_NAME = os.environ.get("APP_NAME")
ENV_NAME = os.environ.get("ENV_NAME")
ENV_TYPE = os.environ.get("ENV_TYPE")
FLOW_RUN_ID = os.environ.get("FLOW_RUN_ID")
TRACE_ID = os.environ.get("TRACE_ID")


class CustomJsonFormatter(jsonlogger.JsonFormatter):
    """
    JSON log formatter that utilizes jsonlogger.JsonFormatter.
    """

    def add_fields(self, log_record, record, message_dict):
        """
        Add fields to a log record, we're adding additional 4 more fields:
        app_name, env_type, env_name and timestamp
        Parameters
        ----------
        log_record
        record
        message_dict

        Returns
        -------

        """
        super().add_fields(log_record, record, message_dict)
        if APP_NAME:
            log_record["app_name"] = APP_NAME
        if FLOW_RUN_ID:
            log_record["flow_run_id"] = FLOW_RUN_ID
        if TRACE_ID:
            log_record["trace_id"] = TRACE_ID
        log_record["env_name"] = ENV_NAME
        log_record["env_type"] = ENV_TYPE
        if not log_record.get("timestamp"):
            # this doesn't use record.created, so it is slightly off
            now = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f")
            log_record["timestamp"] = now

    def format(self, record):
        """
        Override parent method for the case that this log record has already passed through the JsonFormatter
        In that case, simply return the formatted message (trimming trailing newline).
        @param record:
        @return:
        """
        if record.msg and type(record.msg) == bytes:
            return record.msg.decode("utf-8").strip()
        return super().format(record)
