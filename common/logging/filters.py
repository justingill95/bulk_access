import logging

ERROR_FILTERS = {logging.ERROR, logging.FATAL, logging.WARNING, logging.CRITICAL}
INFO_FILTERS = {logging.INFO, logging.DEBUG}


class ErrorFilter(logging.Filter):
    """ErrorFilter"""

    def filter(self, rec):
        """Filter error records"""
        return rec.levelno in ERROR_FILTERS


class InfoFilter(logging.Filter):
    """InfoFilter"""

    def filter(self, rec):
        """Filter info records"""
        return rec.levelno in INFO_FILTERS
