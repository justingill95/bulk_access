import json

import pandas as pd
import pytest
import requests_mock

from sharedlibrary.anaplandal.src.exceptions import AnaplanAPIError

adapter = requests_mock.Adapter()

from typing import Any

from sharedlibrary.anaplandal.src.anaplan_actions import AnaplanActionsClient
from sharedlibrary.anaplandal.tests.data.anaplan_actions_mock_test import (
    ALL_DIMENSIONS_FOR_ADJUST_TIME_SERIES_MODULE_READ,
    DF_INPUT_HEAD_10_AS_DICT_FOR_ADJUST_TIME_SERIES_MODULE_READ,
    DF_ITEMS_DATA,
    DIMENSION_ID,
    DIMENSION_ID_2,
    EXPORT_ID,
    EXPORT_JOB_ID,
    FILE_ID,
    IMPORT_ACTION_POST_1,
    IMPORT_ACTION_POST_2,
    LIST_ID,
    MODEL_ID,
    MODULE_ID,
    MODULE_ID_FOR_READ_MODULE_DATA,
    MODULE_ID_FOR_WRITE_CELL_DATA,
    SUCCESS_ADD_LIST_ITEMS,
    SUCCESS_ADJUST_TIME_SERIES_MODULE_READ,
    SUCCESS_CALENDAR_TYPE,
    SUCCESS_CURRENT_PERIOD,
    SUCCESS_CURRENT_PERIOD_LAST_DAY,
    SUCCESS_DELETE_LIST_ITEMS,
    SUCCESS_EXPORT_ACTION,
    SUCCESS_EXPORT_ACTION_POST_1,
    SUCCESS_EXPORT_ACTION_POST_2,
    SUCCESS_EXPORT_ACTION_POST_3,
    SUCCESS_EXPORT_ACTION_POST_4,
    SUCCESS_FISCAL_YEAR,
    SUCCESS_FISCAL_YEAR_ENDDATE,
    SUCCESS_FISCAL_YEAR_STARTDATE,
    SUCCESS_GET_MODEL_CURRENT_PERIOD,
    SUCCESS_GET_MODEL_DETAILS,
    SUCCESS_GET_VIEW_DIMENSIONS,
    SUCCESS_ITEMS_IN_DIMENSION,
    SUCCESS_LIST_DIMENSION_ITEMS_BY_VIEW_ID,
    SUCCESS_LIST_EXPORTS,
    SUCCESS_LIST_FILES,
    SUCCESS_LIST_IMPORTS,
    SUCCESS_LIST_LINE_ITEMS,
    SUCCESS_LIST_LISTS,
    SUCCESS_LIST_METADATA,
    SUCCESS_LIST_MODULES,
    SUCCESS_LIST_VIEW_FOR_MODULE,
    SUCCESS_LIST_VIEWS,
    SUCCESS_LOOKUP_LINE_ITEM_BY_NAME,
    SUCCESS_MODEL_CALENDAR,
    SUCCESS_READ_EXPORT_METADATA,
    SUCCESS_READ_MODULE_DATA,
    SUCCESS_READ_MODULE_DATA_JSON,
    SUCCESS_READ_TRANSACTIONAL_API_VIEW,
    SUCCESS_REMOVE_SUMMATION_LINES_TIME_SERIES_MODULE,
    SUCCESS_REMOVE_SUMMATION_LINES_TIME_SERIES_MODULE_MOCK_1,
    SUCCESS_REMOVE_SUMMATION_LINES_TIME_SERIES_MODULE_MOCK_2,
    TOKEN_VAL,
    VIEW_ID_2,
    VIEW_ID_3,
    WORKSPACE_ID,
    WRITE_CELL_DATA_GET_1,
    WRITE_CELL_DATA_GET_2,
    WRITE_CELL_DATA_POST_3,
    WRITE_CELL_DATA_SAMPLE_JSON,
    SUCCESS_GET_LIST_ITEMS_include_herarchy_FALSE,
)

# if TYPE_CHECKING:
#     from requests_mock.mocker import Mocker


@requests_mock.Mocker(kw="mock")
def test_list_lists(**kwargs: Any):
    """
    Test the list_lists with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    success_resp_dict = SUCCESS_LIST_LISTS
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8A81B0126EE24B88016FA8F592AB3DF7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lists/",
        text=json.dumps(success_resp_dict),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.list_lists(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response["lists"] == success_resp_dict["lists"], Exception("Unsuccessful list_lists")


@requests_mock.Mocker(kw="mock")
def test_list_line_items(**kwargs: Any):
    """
    Test the list_line_items with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lineItems/",
        text=json.dumps(SUCCESS_LIST_LINE_ITEMS),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.list_line_items(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_LIST_LINE_ITEMS, Exception("Unsuccessful list_line_items")


@requests_mock.Mocker(kw="mock")
def test_lookup_line_item_by_name(**kwargs: Any):
    """
    Test the lookup_line_item_by_name with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lineItems/",
        text=json.dumps(SUCCESS_LIST_LINE_ITEMS),
        status_code=200,
    )

    # Now try to run the method:
    item_name = "quantity"  # NOT CASE SENSITIVE
    response = act_client.lookup_line_item_by_name(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, item_name)
    assert response == SUCCESS_LOOKUP_LINE_ITEM_BY_NAME, Exception("Unsuccessful lookup_line_item_by_name")


@requests_mock.Mocker(kw="mock")
def test_get_model_fiscal_year(**kwargs: Any):
    """
    Test the get_model_fiscal_year with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/modelCalendar/",
        text=json.dumps(SUCCESS_MODEL_CALENDAR),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.get_model_fiscal_year(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_FISCAL_YEAR, Exception("Unsuccessful get_model_fiscal_year")


@requests_mock.Mocker(kw="mock")
def test_get_model_fiscal_year_startdate(**kwargs: Any):
    """
    Test the get_model_fiscal_year_startdate with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/modelCalendar/",
        text=json.dumps(SUCCESS_MODEL_CALENDAR),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.get_model_fiscal_year_startdate(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_FISCAL_YEAR_STARTDATE, Exception("Unsuccessful get_model_fiscal_year_startdate")


@requests_mock.Mocker(kw="mock")
def test_get_model_fiscal_year_enddate(**kwargs: Any):
    """
    Test the get_model_fiscal_year_enddate with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/modelCalendar/",
        text=json.dumps(SUCCESS_MODEL_CALENDAR),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.get_model_fiscal_year_enddate(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_FISCAL_YEAR_ENDDATE, Exception("Unsuccessful get_model_fiscal_year_enddate")


@requests_mock.Mocker(kw="mock")
def test_get_model_calendar_type(**kwargs: Any):
    """
    Test the get_model_calendar_type with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/currentPeriod/",
        text=json.dumps(SUCCESS_CURRENT_PERIOD),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.get_model_calendar_type(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_CALENDAR_TYPE, Exception("Unsuccessful get_model_calendar_type")


@requests_mock.Mocker(kw="mock")
def test_get_model_current_period_last_day(**kwargs: Any):
    """
    Test the get_model_current_period_last_day with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/currentPeriod/",
        text=json.dumps(SUCCESS_CURRENT_PERIOD),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.get_model_current_period_last_day(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_CURRENT_PERIOD_LAST_DAY, Exception("Unsuccessful get_model_current_period_last_day")


@requests_mock.Mocker(kw="mock")
def test_get_model_current_period(**kwargs: Any):
    """
    Test the get_model_current_period with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/currentPeriod/",
        text=json.dumps(SUCCESS_CURRENT_PERIOD),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.get_model_current_period(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_GET_MODEL_CURRENT_PERIOD, Exception("Unsuccessful get_model_current_period")


@requests_mock.Mocker(kw="mock")
def test_list_items_in_dimension(**kwargs: Any):
    """
    Test the list_items_in_dimension with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/dimensions/102000000037/items",
        text=json.dumps(SUCCESS_ITEMS_IN_DIMENSION),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.list_items_in_dimension(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, DIMENSION_ID)
    assert response == SUCCESS_ITEMS_IN_DIMENSION, Exception("Unsuccessful list_items_in_dimension")


@requests_mock.Mocker(kw="mock")
def test_list_items_in_dimension_neg(**kwargs: Any):
    """
    Test the list_items_in_dimension with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/dimensions/102000000037/items",
        text=json.dumps(SUCCESS_ITEMS_IN_DIMENSION),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.list_items_in_dimension(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, DIMENSION_ID)
    assert response == SUCCESS_ITEMS_IN_DIMENSION, Exception("Unsuccessful list_items_in_dimension")


@requests_mock.Mocker(kw="mock")
def test_list_modules(**kwargs: Any):
    """
    Test the list_modules with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/modules/",
        text=json.dumps(SUCCESS_LIST_MODULES),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.list_modules(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_LIST_MODULES, Exception("Unsuccessful list_modules")


@requests_mock.Mocker(kw="mock")
def test_list_exports(**kwargs: Any):
    """
    Test the list_exports with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/exports/",
        text=json.dumps(SUCCESS_LIST_EXPORTS),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.list_exports(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_LIST_EXPORTS, Exception("Unsuccessful list_exports")


@requests_mock.Mocker(kw="mock")
def test_list_imports(**kwargs: Any):
    """
    Test the list_imports with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/imports/",
        text=json.dumps(SUCCESS_LIST_IMPORTS),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.list_imports(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_LIST_IMPORTS, Exception("Unsuccessful list_imports")


@requests_mock.Mocker(kw="mock")
def test_list_files(**kwargs: Any):
    """
    Test the list_files with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/files/",
        text=json.dumps(SUCCESS_LIST_FILES),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.list_files(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_LIST_FILES, Exception("Unsuccessful list_files")


@requests_mock.Mocker(kw="mock")
def test_list_views(**kwargs: Any):
    """
    Test the list_views with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/views/",
        text=json.dumps(SUCCESS_LIST_VIEWS),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.list_views(TOKEN_VAL, WORKSPACE_ID, MODEL_ID)
    assert response == SUCCESS_LIST_VIEWS, Exception("Unsuccessful list_views")


@requests_mock.Mocker(kw="mock")
def test_list_views_for_module(**kwargs: Any):
    """
    Test the list_views_for_module with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/modules/102000000012/views",
        text=json.dumps(SUCCESS_LIST_VIEW_FOR_MODULE),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.list_views_for_module(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, MODULE_ID)
    assert response == SUCCESS_LIST_VIEW_FOR_MODULE, Exception("Unsuccessful list_views_for_module")


@requests_mock.Mocker(kw="mock")
def test_get_default_view_dimensions(**kwargs: Any):
    """
    Test the get_default_view_dimensions with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/views/102000000012/",
        text=json.dumps(SUCCESS_GET_VIEW_DIMENSIONS),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.get_default_view_dimensions(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, MODULE_ID)
    assert response == SUCCESS_GET_VIEW_DIMENSIONS, Exception("Unsuccessful get_default_view_dimensions")


@requests_mock.Mocker(kw="mock")
def test_get_model_details(**kwargs: Any):
    """
    Test the get_model_details with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D",
        text=json.dumps(SUCCESS_GET_MODEL_DETAILS),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.get_model_details(TOKEN_VAL, MODEL_ID)
    assert response == SUCCESS_GET_MODEL_DETAILS, Exception("Unsuccessful get_model_details")


@requests_mock.Mocker(kw="mock")
def test_get_list_items(**kwargs: Any):
    """
    Test the get_list_items with mocks
    parameter used include_hierarchy=False
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lists/101000000003/items",
        text=json.dumps(SUCCESS_GET_LIST_ITEMS_include_herarchy_FALSE),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.get_list_items(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, LIST_ID, include_hierarchy=False)
    assert response == SUCCESS_GET_LIST_ITEMS_include_herarchy_FALSE, Exception("Unsuccessful get_list_items")


@requests_mock.Mocker(kw="mock")
def test_get_list_metadata(**kwargs: Any):
    """
    Test the get_list_metadata with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lists/101000000003",
        text=json.dumps(SUCCESS_LIST_METADATA),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.get_list_metadata(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, LIST_ID)
    assert response == SUCCESS_LIST_METADATA["metadata"], Exception("Unsuccessful get_list_metadata")


@requests_mock.Mocker(kw="mock")
def test_get_list_metadata_no_json(**kwargs: Any):
    """
    Test the get_list_metadata with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "no content" response :
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lists/101000000003",
        status_code=204,
    )

    # Now try to run the method:
    with pytest.raises(AnaplanAPIError) as exception_info:
        act_client.get_list_metadata(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, LIST_ID)
    assert "json" in str(exception_info.value)


@requests_mock.Mocker(kw="mock")
def test_get_list_metadata_no_metadata(**kwargs: Any):
    """
    Test the get_list_metadata with mocks
    :return:
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response, but with missing metadata:
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lists/101000000003",
        text=json.dumps(
            {
                "meta": {
                    "schema": "https://api.r2p2.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/objects/list"
                },
                "status": {"code": 200, "message": "Success"},
            }
        ),
        status_code=200,
    )

    # Now try to run the method:
    with pytest.raises(AnaplanAPIError) as exception_info:
        act_client.get_list_metadata(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, LIST_ID)
    assert "metadata" in str(exception_info.value)


@pytest.mark.skip(reason="temporery, till Ofir will fix it")
@requests_mock.Mocker(kw="mock")
def test_find_module_by_line_item(**kwargs: Any):
    """
    Test the find_module_by_line_item with mocks

    :return:
    todo: consider deleting and use find_line_item_by_name instad...
    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lineItems/",
        text=json.dumps(SUCCESS_LIST_LINE_ITEMS),
        status_code=200,
    )

    # Now try to run the method:
    item_name = "Quantity"  #  CASE SENSITIVE !!
    response = act_client.find_module_by_line_item(TOKEN_VAL, MODEL_ID, item_name)
    assert response == SUCCESS_LOOKUP_LINE_ITEM_BY_NAME, Exception("Unsuccessful find_module_by_line_item")


@requests_mock.Mocker(kw="mock")
def test_list_dimension_items_by_view_id(**kwargs: Any):
    """
    Test the list_dimension_items_by_view_id with mocks

    :return:

    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/views/102000000012/dimensions/20000000003/items",
        text=json.dumps(SUCCESS_LIST_DIMENSION_ITEMS_BY_VIEW_ID),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.list_dimension_items_by_view_id(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, VIEW_ID_2, DIMENSION_ID_2)
    assert response == SUCCESS_LIST_DIMENSION_ITEMS_BY_VIEW_ID, Exception(
        "Unsuccessful list_dimension_items_by_view_id"
    )


@requests_mock.Mocker(kw="mock")
def test_read_transactional_api_view(**kwargs: Any):
    """
    Test the read_transactional_api_view with mocks

    :return:

    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/views/102000000015/data",
        text=(SUCCESS_READ_TRANSACTIONAL_API_VIEW),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.read_transactional_api_view(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, VIEW_ID_3)
    assert response == SUCCESS_READ_TRANSACTIONAL_API_VIEW.encode(), Exception(
        "Unsuccessful read_transactional_api_view"
    )


@requests_mock.Mocker(kw="mock")
def test_read_export_metadata(**kwargs: Any):
    """
    Test the read_export_metadata with mocks

    :return:

    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/exports/116000000001/",
        text=json.dumps(SUCCESS_READ_EXPORT_METADATA),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.read_export_metadata(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, EXPORT_ID)
    assert response == SUCCESS_READ_EXPORT_METADATA, Exception("Unsuccessful read_export_metadata")


@requests_mock.Mocker(kw="mock")
def test_add_list_items(**kwargs: Any):
    """
    Test the add_list_items with mocks

    :return:

    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.post(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lists/101000000003/items/",
        text=json.dumps(SUCCESS_ADD_LIST_ITEMS),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.add_list_items(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, LIST_ID, pd.DataFrame(data=DF_ITEMS_DATA))
    assert response == None, Exception("Unsuccessful add_list_items")


@requests_mock.Mocker(kw="mock")
def test_delete_list_items(**kwargs: Any):
    """
    Test the delete_list_items with mocks

    :return:

    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.post(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lists/101000000003/items?delete=true",
        text=json.dumps(SUCCESS_DELETE_LIST_ITEMS),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.delete_list_items(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, LIST_ID, SUCCESS_DELETE_LIST_ITEMS)
    assert response == None, Exception("Unsuccessful delete_list_items")


@requests_mock.Mocker(kw="mock")
def test_read_module_data(**kwargs: Any):
    """
    Test the read_module_data with mocks

    :return:

    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/views/102000000038/data",
        text=json.dumps(SUCCESS_READ_MODULE_DATA),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.read_module_data(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, None, MODULE_ID_FOR_READ_MODULE_DATA)
    assert response.to_json() == SUCCESS_READ_MODULE_DATA_JSON, Exception("Unsuccessful read_module_data")


def test_adjust_time_series_module_read(**kwargs: Any):
    """
    Test the adjust_time_series_module_read method

    :return:

    """
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")

    response = act_client.adjust_time_series_module_read(
        pd.DataFrame.from_dict(DF_INPUT_HEAD_10_AS_DICT_FOR_ADJUST_TIME_SERIES_MODULE_READ),
        ALL_DIMENSIONS_FOR_ADJUST_TIME_SERIES_MODULE_READ,
    )
    assert response.head(10).to_dict() == SUCCESS_ADJUST_TIME_SERIES_MODULE_READ, Exception(
        "Unsuccessful adjust_time_series_module_read"
    )


@requests_mock.Mocker(kw="mock")
def test_remove_summation_lines_time_series_module(**kwargs: Any):
    """
    Test the remove_summation_lines_time_series_module with mocks

    :return:

    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lists/101000000005/items",
        text=json.dumps(SUCCESS_REMOVE_SUMMATION_LINES_TIME_SERIES_MODULE_MOCK_1),
        status_code=200,
    )
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lists/101000000003/items",
        text=json.dumps(SUCCESS_REMOVE_SUMMATION_LINES_TIME_SERIES_MODULE_MOCK_2),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.remove_summation_lines_time_series_module(
        TOKEN_VAL,
        WORKSPACE_ID,
        MODEL_ID,
        pd.DataFrame.from_dict(SUCCESS_ADJUST_TIME_SERIES_MODULE_READ),
        ALL_DIMENSIONS_FOR_ADJUST_TIME_SERIES_MODULE_READ,
    )
    assert (
        response[0].head(10).to_dict(),
        response[1],
    ) == SUCCESS_REMOVE_SUMMATION_LINES_TIME_SERIES_MODULE, Exception(
        "Unsuccessful remove_summation_lines_time_series_module"
    )


@requests_mock.Mocker(kw="mock")
def test_export_action(**kwargs: Any):
    """
    Test the export_action with mocks

    :return:

    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.post(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/exports/116000000001/tasks",
        text=json.dumps(SUCCESS_EXPORT_ACTION_POST_1),
        status_code=200,
    )
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/exports/116000000001/tasks/B32A017A440A4D2FAF70CAE06F849F26",
        text=json.dumps(SUCCESS_EXPORT_ACTION_POST_2),
        status_code=200,
    )
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/files/116000000001/chunks",
        text=json.dumps(SUCCESS_EXPORT_ACTION_POST_3),
        status_code=200,
    )
    m.get(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/files/116000000001/chunks/0",
        text=SUCCESS_EXPORT_ACTION_POST_4,
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.export_action(TOKEN_VAL, WORKSPACE_ID, MODEL_ID, EXPORT_ID)
    assert response.head(10).to_dict() == SUCCESS_EXPORT_ACTION, Exception("Unsuccessful export_action")


@requests_mock.Mocker(kw="mock")
def test_import_action(**kwargs: Any):
    """
    Test the import_action with mocks

    :return:

    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.post(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/files/113000000018",
        text=json.dumps(IMPORT_ACTION_POST_1),
        status_code=200,
    )
    m.put(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/files/113000000018/chunks/0",
        text=None,
        status_code=204,
    )
    m.post(
        "https://api.anaplan.com/2/0/workspaces/8a81b0126ee24b88016fa8f592ab3df7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/imports/112000000019/tasks",
        text=json.dumps(IMPORT_ACTION_POST_2),
        status_code=200,
    )

    # Now try to run the method:

    response = act_client.import_action(
        TOKEN_VAL, WORKSPACE_ID.lower(), MODEL_ID, FILE_ID, EXPORT_JOB_ID, "Countries.csv"
    )
    assert response == None, Exception("Unsuccessful import_action")


@requests_mock.Mocker(kw="mock")
def test_write_cell_data(**kwargs: Any):
    """
    Test the write_cell_data with mocks

    :return:

    """
    m = kwargs["mock"]
    # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
    act_client = AnaplanActionsClient(endpoint="https://api.anaplan.com/2/0")
    # Generate a "success" response :
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/views/102000000035/",
        text=WRITE_CELL_DATA_GET_1,
        status_code=200,
    )
    m.get(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/modules/102000000035/lineItems/",
        text=WRITE_CELL_DATA_GET_2,
        status_code=200,
    )
    m.post(
        "https://api.anaplan.com/2/0/models/CE7A8EDE5CCC46DE976E6DA3549F056D/modules/102000000035/data",
        text=WRITE_CELL_DATA_POST_3,
        status_code=200,
    )

    # Now try to run the method:
    response = act_client.write_cell_data(
        TOKEN_VAL, WORKSPACE_ID, MODEL_ID, MODULE_ID_FOR_WRITE_CELL_DATA, pd.DataFrame(data=WRITE_CELL_DATA_SAMPLE_JSON)
    )
    assert response == None, Exception("Unsuccessful write_cell_data")


# @requests_mock.Mocker(kw="mock")
# def test_lookup_list_by_item_name(**kwargs):
#     """
#     Test the lookup_list_by_item_name with mocks
#       todo: check how to use mock since ew have multiple calls to get all items for each list...check for wildcard in requests_mock...or ask Tal
#     :return:
#     """
#     m = kwargs["mock"]
#     # Generate an AuthClient instance with some basepoint (not important what it is as we are mocking responses):
#     act_client = AnaplanActionsClient(endpoint='https://api.anaplan.com/2/0')
#     # Generate a "success" response :
#     success_resp_dict = SUCCESS_LIST_LISTS
#     m.get('https://api.anaplan.com/2/0/workspaces/8A81B0126EE24B88016FA8F592AB3DF7/models/CE7A8EDE5CCC46DE976E6DA3549F056D/lists/',
#           text=json.dumps(success_resp_dict), status_code=200)
#
#     # Now try to run the method:
#     item_name = 'credit card'
#     response = act_client.lookup_list_by_item_name(TOKEN_VAL, WORKSPACE_ID,MODEL_ID,item_name)
#     assert response == SUCCESS_LOOKUP_LINE_ITEM_BY_NAME , Exception("Unsuccessful lookup_module_by_name")

if __name__ == "__main__":

    test_list_lists()
    test_lookup_line_item_by_name()
    test_get_model_fiscal_year()
    test_get_model_fiscal_year_startdate()
    test_get_model_fiscal_year_enddate()
    test_get_model_calendar_type()
    test_get_model_current_period_last_day()
    test_get_model_current_period()
    test_list_items_in_dimension()
    test_list_line_items()
    test_list_modules()
    test_list_exports()
    test_list_imports()
    test_list_files()
    test_list_views()
    test_list_views_for_module()
    test_get_model_details()
    test_get_list_items()
    test_find_module_by_line_item()
    test_list_dimension_items_by_view_id()
    test_get_default_view_dimensions()
    test_read_transactional_api_view()
    test_read_export_metadata()
    test_delete_list_items()
    test_read_module_data()
    # test_lookup_list_by_item_name()
    test_adjust_time_series_module_read()
    test_remove_summation_lines_time_series_module()
    test_import_action()
    test_export_action()
    test_add_list_items()
    test_delete_list_items()
    test_write_cell_data()
    print("Done!")
