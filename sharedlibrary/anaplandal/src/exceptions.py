class AnaplanImportError(Exception):
    """
    Anaplan Import Error
    """

    pass


class AnaplanExportError(Exception):
    """
    Anaplan Export Error
    """

    pass


class AnaplanAPIError(Exception):
    """
    Anaplan API Error
    """

    pass


class AnaplanUnauthorizedException(Exception):
    """
    Anaplan Unauthorized Error
    """

    pass


class AnaplanMissmatchColumnsError(Exception):
    """
    Columns/list in new data does not match modules columns
    """

    pass


class AnaplanValueError(Exception):
    """
    Anaplan Model Value Error.
    """

    pass


class AnaplanAutomlPredictionError(Exception):
    """
    Anaplan automl prediction Error.
    """

    pass
