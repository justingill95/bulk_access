import json
from typing import TYPE_CHECKING, Dict, Optional

import requests
from requests.auth import HTTPBasicAuth
from retrying import retry

from common.logging.plogger import PLogger
from sharedlibrary.anaplandal.src.exceptions import AnaplanUnauthorizedException

if TYPE_CHECKING:
    from requests.adapters import Response

logger = PLogger(__name__)


def _retry_if_request_failed(response: "Response") -> bool:
    """
    Retry request if it failed.
    :param response: previous response
    :return:
    """
    return (response is not None) and (response.status_code >= 400)


class AnaplanAuthClient(object):
    """
    Class represents Anaplan client for authentication process.
    """

    def __init__(self, base_endpoint: str):
        """
        Constructor
        :param base_endpoint: base endpoint
        """
        self.auth_endpoint = "{base_endpoint}/token/authenticate".format(base_endpoint=base_endpoint)
        self.validation_endpoint = "{base_endpoint}/token/validate".format(base_endpoint=base_endpoint)
        self.meta_data_endpoint = "{base_endpoint}/2/0/users/me".format(base_endpoint=base_endpoint)
        self.logout_endpoint = "{base_endpoint}/logout".format(base_endpoint=base_endpoint)

    def validate_token(self, token_value: str, extra_headers: Optional[Dict[str, str]] = None) -> bool:
        """
        Validate token
        :param token_value:
        :param extra_headers:
        :return: returns True if token is valid, False otherwise.
        """
        headers = self.__generate_auth_header(token_value, extra_headers=extra_headers)
        response = self.__make_get_request(url=self.validation_endpoint, headers=headers)
        if not response.ok:
            logger.error(
                "failed validate user token. status code: {code}, token: {token}. content: {content}".format(
                    code=response.status_code, token=token_value, content=response.content
                ),
                extra={"status_code": response.status_code},
            )
            return False
        return True

    def get_user_metadata(self, token_value: str, extra_headers: Optional[Dict[str, str]] = None) -> Dict[str, str]:
        """
        Get user metadata
        :param token_value:
        :param extra_headers:
        :return: user metadata.
        """
        headers = self.__generate_auth_header(token_value, extra_headers=extra_headers)
        response = self.__make_get_request(url=self.meta_data_endpoint, headers=headers)
        if not response.ok:
            logger.error(
                "failed to fetch user metadata. status code: {code}, token: {token}".format(
                    code=response.status_code, token=token_value
                ),
                extra={"status_code": response.status_code},
            )
            logger.debug("response content: {content}".format(content=response.content))
            return {}
        user_data = response.json().get("user")
        logger.info("user data from yeti response: {user_data}".format(user_data=user_data))
        return user_data

    def logout(self, token_value: str, extra_headers: Optional[Dict[str, str]] = None) -> bool:
        """
        Logout
        :param token_value:
        :param extra_headers:
        :return:
        """
        headers = self.__generate_auth_header(token_value, extra_headers=extra_headers)
        response = self.__make_post_request(url=self.logout_endpoint, headers=headers)
        return response.ok

    def __generate_auth_header(
        self, token: Optional[str] = None, extra_headers: Optional[Dict[str, str]] = None
    ) -> Dict[str, str]:
        """
        Genereates authentication header.
        :param token:
        :param extra_headers:
        :return:
        """
        headers: Dict[str, str] = {}
        if extra_headers:
            headers.update(extra_headers)
        if token:
            headers["Authorization"] = "{token}".format(token=token)
        return headers

    @retry(stop_max_attempt_number=3, retry_on_result=_retry_if_request_failed)
    def __make_get_request(self, url: str, headers: Dict[str, str]) -> "Response":
        """
        A method that makes get request.
        :param url:
        :param headers:
        :return:
        """
        logger.debug("sending GET request to {url} with headers: {headers}".format(url=url, headers=headers))
        return requests.get(url, headers=headers)

    @retry(stop_max_attempt_number=3, retry_on_result=_retry_if_request_failed)
    def __make_post_request(
        self,
        url: str,
        headers: Dict[str, str],
        auth: Optional["HTTPBasicAuth"] = None,
        data_json: Optional[Dict[str, str]] = None,
    ) -> "Response":
        """
        A method that makes post request.
        :param url:
        :param headers:
        :param auth:
        :param data_json:
        :return:
        """
        logger.debug(
            "sending POST request to {url} with auth: {auth}, headers: {headers} and body: {body}".format(
                url=url, headers=headers, body=data_json, auth=auth
            )
        )
        return requests.post(url, headers=headers, json=data_json, auth=auth)

    def authenticate(self, username: str, password: str) -> str:
        """
        Authentication method
        :param username:
        :param password:
        :return:
        """
        auth = HTTPBasicAuth(username, password)
        headers = self.__generate_auth_header()
        response = self.__make_post_request(self.auth_endpoint, auth=auth, headers=headers)
        response_dict = json.loads(response.content)
        http_status_code = response.status_code
        status = response_dict.get("status")
        if http_status_code == requests.codes.unauthorized:
            logger.error(
                "failed to authenticate user token. status code: {code}, content: {content}".format(
                    code=response.status_code, content=response.content
                ),
                extra={"status_code": response.status_code},
            )
            raise AnaplanUnauthorizedException()
        elif not (http_status_code == requests.codes.created and status == "SUCCESS"):
            raise Exception(
                "Got Unknown status code %s while trying to authenticate user to Anaplan. "
                "Got status: %s, message: %s." % (http_status_code, status, response_dict.get("statusMessage"))
            )
        return response_dict["tokenInfo"]
