# pyright: reportUnknownVariableType=false
# pyright: reportUnknownArgumentType=false

import json
import time
from itertools import chain
from logging import getLogger
from tempfile import TemporaryFile
from typing import IO, TYPE_CHECKING, Any, Callable, Dict, List, Optional, Set, Tuple, Type, Union

import pandas as pd
import requests
from requests import JSONDecodeError

if TYPE_CHECKING:
    from numpy import bool_
    from numpy.typing import NDArray
    from requests.adapters import Response

logger = getLogger(__name__)

from sharedlibrary.anaplandal.src.exceptions import (
    AnaplanAPIError,
    AnaplanExportError,
    AnaplanImportError,
    AnaplanMissmatchColumnsError,
)

SLEEP_POLL_SECS = 5
POST_REQ = "POST"
GET_REQ = "GET"
PUT_REQ = "PUT"
DELETE_REQ = "DELETE"

CELL_WRITE_CHUNK_SIZE = 1000
STANDARD_DIMS_SET = {"Time", "Version", "Users"}
# PATH_TO_MY_CREDS = os.path.dirname(os.path.dirname(__file__))


class AnaplanActionsClient:
    """
    A class represents a client that handles Anaplan data access Actions.
    Implements:
        * Authentication - uses user name + password
        * Import action trigger - import a file into an import action
        * Export action trigger - read a file from an export action
    """

    def __init__(self, endpoint: str = "https://api.anaplan.com/2/0/", save_responses: bool = False):
        """
        Constructor
        :param endpoint: Anaplan API url.
        :param save_responses: If True, we save the requests responses in self.resp_hist. This is used to build unit tessts.
        """
        self.endpoint = endpoint.strip("/")
        self.resp_hist: List[Tuple[str, str, "Response"]] = []
        self.save_responses = save_responses

    def _post(self, url: str, *args: Any, **kwargs: Any):
        """
        A method-wrapper for the requests.post. This is to enable patching during testing.
        :param url: The url to call POST to
        :param args: Additional positional args
        :param kwargs: Additional named args
        :return: The post result
        """
        resp = requests.post(url, *args, **kwargs)
        if self.save_responses:
            self.resp_hist.append(("POST", url, resp))
        return resp

    def _get(self, url: str, *args: Any, **kwargs: Any) -> "Response":
        """
        A method-wrapper for the requests.get. This is to enable patching during testing.
        :param url: The url to call GET to
        :param args: Additional positional args
        :param kwargs: Additional named args
        :return: The get result
        """
        resp = requests.get(url, *args, **kwargs)
        if self.save_responses:
            self.resp_hist.append(("GET", url, resp))
        return resp

    def _put(self, url: str, *args: Any, **kwargs: Any):
        """
        A method-wrapper for the requests.put. This is to enable patching during testing.
        :param url: The url to call PUT to
        :param args: Additional positional args
        :param kwargs: Additional named args
        :return: The put result
        """
        resp = requests.put(url, *args, **kwargs)
        if self.save_responses:
            self.resp_hist.append(("PUT", url, resp))
        return resp

    def _delete(self, url: str, *args: Any, **kwargs: Any):
        """
        A method-wrapper for the requests.put. This is to enable patching during testing.
        :param url: The url to call PUT to
        :param args: Additional positional args
        :param kwargs: Additional named args
        :return: The put result
        """
        resp = requests.delete(url, *args, **kwargs)
        if self.save_responses:
            self.resp_hist.append(("DELETE", url, resp))
        return resp

    def _anaplan_api_call(
        self,
        rest_verb: str,
        endpoint: str,
        auth_token: str,
        headers: Optional[Dict[str, str]] = None,
        xac_client: str = "Dream_v1",
        exception_class: Type[Exception] = Exception,
        exception_message: Union[str, None] = None,
        **kwargs: Any,
    ) -> "Response":
        """
        Run an action on the Anaplan REST API.
        :param rest_verb: GET/POST/PUT (as a string)
        :param endpoint: The endpoint URL
        :param auth_token: The auth token to use
        :param headers: Any headers to send (or None if not needed). Note the method adds the Authorization value by itself
        :param xac_client: project name for XA_ClientConnet by default al pai calls will be currenlty under "Dream"
        :param exception_class: What exception to raise if there is an error
        :param exception_message: A message for the exception. If None, the default will be used
        :param kwargs: Any additional named args are sent to the request
        :return: The response result
        """
        if headers is None:
            headers = {}
        headers["Authorization"] = "AnaplanAuthToken " + auth_token
        headers["X-AConnect-Client"] = xac_client
        # Send the request:
        if rest_verb == GET_REQ:
            response = self._get(endpoint, headers=headers, **kwargs)
        elif rest_verb == POST_REQ:
            response = self._post(endpoint, headers=headers, **kwargs)
        elif rest_verb == PUT_REQ:
            response = self._put(endpoint, headers=headers, **kwargs)
        elif rest_verb == DELETE_REQ:
            response = self._delete(endpoint, headers=headers, **kwargs)
        else:
            raise ValueError(f"Unknown REST verb {rest_verb}")

        # Check if response is a success:
        if not response.ok:
            if exception_message is None:
                exception_message = f"Exception in {rest_verb}"
            raise exception_class(f"{exception_message}: {response.content}")
        return response

    def _setup_import_action(self, auth_token: str, workspace_id: str, model_id: str, file_id: str) -> bool:
        """
        Setup Anaplan import action.
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param file_id:
        :return: returns True if the setup finished successfully.
        """
        _ = self._anaplan_api_call(
            POST_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/files/{file_id}",
            auth_token=auth_token,
            headers={"Content-Type": "application/json"},
            json={"id": str(file_id), "chunkCount": 1},
            exception_class=AnaplanImportError,
            exception_message="initiate import failed",
        )
        return True

    def _push_file_for_import(
        self, auth_token: str, workspace_id: str, model_id: str, file_id: str, data_to_send: str
    ) -> bool:
        """
        Push a file for Anaplan import action.
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param file_id:
        :param data_to_send:
        :return: returns True if file pushed successfully.
        """
        _ = self._anaplan_api_call(
            PUT_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/files/{file_id}/chunks/0",
            headers={"Content-Type": "application/octet-stream"},
            data=data_to_send,
            auth_token=auth_token,
            exception_class=AnaplanImportError,
            exception_message="push file error",
        )
        return True

    def _trigger_import_action(self, auth_token: str, workspace_id: str, model_id: str, import_job_id: str) -> bool:
        """
        Trigger Anaplan import action.
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param import_job_id:
        :return: returns True if import action was successful.
        """
        _ = self._anaplan_api_call(
            POST_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/imports/{import_job_id}/tasks",
            headers={"Content-Type": "application/json"},
            json={"localeName": "en_US"},
            auth_token=auth_token,
            exception_class=AnaplanImportError,
            exception_message="trigger import error",
        )
        return True

    def trigger_export_action(self, auth_token: str, workspace_id: str, model_id: str, export_job_id: str) -> str:
        """
        Trigger Anaplan export action.
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param export_job_id:
        :return: returns response task id if the action was successful.
        """
        response = self._anaplan_api_call(
            POST_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/exports/{export_job_id}/tasks",
            headers={"Content-Type": "application/json"},
            json={"localeName": "en_US"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="trigger export error",
        )
        task = response.json().get("task")["taskId"]
        return task

    def poll_export_action(
        self, auth_token: str, workspace_id: str, model_id: str, export_job_id: str, task_id: str
    ) -> Union[None, Dict[str, str]]:
        """
        Poll the monitor for a task to see if it is ready to export the file

        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param export_job_id:
        :param task_id:
        :return: returns None if the result is not yet ready, If something is wrong raises an exception,
        If done returns the result dict
        """
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/exports/{export_job_id}/tasks/{task_id}",
            headers={"Content-Type": "application/json"},
            json={"localeName": "en_US"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="wait for export error",
        )

        # Check the result - is the task complete?
        task_res = response.json().get("task")
        if task_res["taskState"] == "COMPLETE":
            return task_res["result"]
        elif task_res["taskState"] == "CANCELED":
            raise AnaplanExportError(f"export task cancelled")
        else:
            # Other states are in transit
            return None

    def _get_download_file_chunks(
        self, auth_token: str, workspace_id: str, model_id: str, file_id: str
    ) -> List[Dict[str, str]]:
        """
        Get file chunks.
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param file_id:
        :return: response chunks if the response was OK.
        """
        # /workspaces/{workspaceID}/models/{modelID}/files/{fileID}/chunks
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/files/{file_id}/chunks",
            headers={"Content-Type": "application/json"},
            json={"localeName": "en_US"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="download export file error",
        )

        # Export files contains the chunks like this:
        # "chunks": [
        #     {
        #         "id": "0",
        #         "name": "Chunk 0"
        #     },
        #     {
        #         "id": "1",
        #         "name": "Chunk 1"
        #     },
        #     {
        #         "id": "2",
        #         "name": "Chunk 2"
        #     },
        #     {
        #         "id": "3",
        #         "name": "Chunk 3"
        #     }
        return response.json()["chunks"]

    def _download_chunk(
        self, auth_token: str, workspace_id: str, model_id: str, file_id: str, chunk: Dict[str, str]
    ) -> "bytes":
        """
        Download file chunk
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param file_id:
        :param chunk:
        :rtype:
        """
        # /workspaces/{workspaceID}/models/{modelID}/files/{fileID}/chunks/{chunkID}
        chunk_id = chunk["id"]
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/files/{file_id}/chunks/{chunk_id}",
            headers={"Accept": "application/octet-stream"},
            json={"localeName": "en_US"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message=f"download export file chunk {chunk_id} error",
        )
        return response.content

    def get_export_file(
        self,
        auth_token: str,
        workspace_id: str,
        model_id: str,
        task_res: Dict[str, str],
        read_func: Callable[["IO[bytes]"], Any] = pd.read_csv,
        *read_args: Any,
        **read_kwargs: Any,
    ):
        """
        Download the file generated by the export task
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param task_res:
        :param read_func:
        :param read_args:
        :param read_kwargs:
        :return:
        """
        if task_res["successful"] != True:
            raise AnaplanExportError(f"Bad export task status {task_res['successful']}")

        file_id = task_res["objectId"]
        chunks = self._get_download_file_chunks(auth_token, workspace_id, model_id, file_id)

        with TemporaryFile(suffix=".csv") as fp:
            for chunk in chunks:
                fp.write(self._download_chunk(auth_token, workspace_id, model_id, file_id, chunk))
            fp.seek(0)
            res_df = read_func(fp, *read_args, **read_kwargs)
        return res_df

    def import_action(
        self, auth_token: str, workspace_id: str, model_id: str, file_id: str, import_job_id: str, data_to_send: str
    ):
        """
         Run the import action. The steps are:
             1. Set the number of chunks - we always use a single chunk upload at this point
             2. Upload the file - this is done with a PUT request to the file_id
             3. Start the import task. If import_job_id is a list, run all actions in the list by the order in the list
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param file_id:
        :param import_job_id:
        :param data_to_send:
        :return:
        """
        self._setup_import_action(auth_token, workspace_id, model_id, file_id)
        self._push_file_for_import(auth_token, workspace_id, model_id, file_id, data_to_send)
        if isinstance(import_job_id, list):
            for action_id in import_job_id:
                self._trigger_import_action(auth_token, workspace_id, model_id, action_id)
                time.sleep(2)
        else:
            self._trigger_import_action(auth_token, workspace_id, model_id, import_job_id)

    def export_action(self, auth_token: str, workspace_id: str, model_id: str, export_job_id: str) -> "pd.DataFrame":
        """
        The export file process according to API reference: https://anaplanbulkapi20.docs.apiary.io/#reference/export
            1. POST the export task to run the export.
            2. Monitor each export task using the ID that was returned when you started the export. If you are exporting a
               large file, it might take some time.
            3. When the export is complete, download the files.
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :return: DataFrame with the export result
        """
        # Consider: Get the metadata for the export: /workspaces/{workspaceID}/models/{modelID}/exports/{exportID}
        task_id = self.trigger_export_action(auth_token, workspace_id, model_id, export_job_id)
        while True:
            task_res = self.poll_export_action(auth_token, workspace_id, model_id, export_job_id, task_id)
            if task_res is None:
                logger.info(f"Waiting for task...")
                time.sleep(SLEEP_POLL_SECS)
            else:
                logger.info(f"Export to file done!")
                break
        res_df = self.get_export_file(auth_token, workspace_id, model_id, task_res, header=0)
        return res_df

    def read_export_metadata(
        self, auth_token: str, workspace_id: str, model_id: str, export_job_id: str
    ) -> Dict[str, str]:
        """
        Read export metadata
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param export_job_id:
        :return: response json content.
        """
        # /workspaces/{workspaceID}/models/{modelID}/exports/{exportID}
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/exports/{export_job_id}/",
            headers={"Content-Type": "application/json"},
            json={"localeName": "en_US"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="download export file error",
        )
        return response.json()

    def read_transactional_api_view(
        self,
        auth_token: str,
        workspace_id: Optional[str],
        model_id: str,
        view_id: str,
        accept_type: str = "text/csv",
        pages: Optional[List[str]] = None,
    ) -> Union[Dict[str, str], bytes]:
        """
        Reads a view using the transactional API
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param view_id:
        :param accept_type: If 'application/json' returns the decoded JSON return answer. If 'text/csv' returns CSV
        :param pages: indicate the value for the specific page needed to be read
        :return: response json content / CSV text
        """
        # Set the params to include the format and the pages (if requesting a specific page):
        params: Dict[str, Union[str, List[str]]] = {"format": "v1"}
        if pages:
            params["pages"] = pages

        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/models/{model_id}/views/{view_id}/data",
            headers={"Accept": accept_type},
            json={"localeName": "en_US"},
            auth_token=auth_token,
            params=params,
            exception_class=AnaplanExportError,
            exception_message="Transactional API error",
        )

        if accept_type == "application/json":
            try:
                return response.json()

            except:

                #  response.content.replace(b',,',b',')
                # return response.content.replace(b',,',b',')

                return json.loads(
                    response.content.replace(b",,", b",")
                )  # todo: removing double commas due to an anaplan bug...discussed woth Tal an email was sent to dave clark
        return response.content

    def get_parent_names(
        self,
        auth_token: str,
        workspace_id: str,
        model_id: str,
        view_dimensions: Dict[str, List[Dict[str, int]]],
        list_items_dict: Optional[Dict[str, Dict[str, str]]] = None,
    ) -> Dict[str, Set[str]]:
        """
        Returns a dict of {name: [set of parent items]} for all lists in the dimensions of a view.
        Ignores the 'Line Items' value - no parents are needed for that
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param view_dimensions: The result of self.get_default_view_dimensions()
        :param list_items_dict: A dict of {dimension_id: result from self.get_list_items() on this dimension}. If None,
            we call get_list_items() on each dimension.
        :return: a dict of {name: [set of parent items]} for all lists in the dimensions of a view.
        """
        res = {}
        for dim in ["columns", "rows", "pages"]:
            items = view_dimensions.get(dim)
            if items is not None:
                for col_item in items:
                    # col_item is something like this: {'name': 'House ID', 'id': '101000000001'}
                    if col_item["name"] == "Line Items":
                        # Skip!
                        continue
                    if (list_items_dict is not None) and (col_item["id"] in list_items_dict):
                        col_list_items = list_items_dict[col_item["id"]]
                    else:
                        col_list_items = self.get_list_items(auth_token, workspace_id, model_id, col_item["id"])
                    res[col_item["name"]] = {i.get("parent") for i in col_list_items["listItems"] if "parent" in i}
        return res

    def get_default_view_dimensions(
        self,
        auth_token: str,
        workspace_id: Optional[str],
        model_id: str,
        module_id: str,
    ) -> Dict[str, List[Dict[str, str]]]:
        """
        Retrieve the dimensions for a module or a view
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param module_id: The module_id or view_id in a module to retrieve dimensions for
        :return:
            Dict[str, Union[List[Dict[str, str]], str]]: this is the actual
             object that we get back, but the current code does not validate if the value is a list or a string
        """
        # /models/{modelId}/views/{viewId}
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/models/{model_id}/views/{module_id}/",
            headers={"Accept": "application/json"},
            json={"localeName": "en_US"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Module dimensions API error",
        )
        return response.json()

    def bulk_pull_request_start(
        self,
        auth_token: str,
        workspace_id: Optional[str],
        model_id: str,
        view_id: str
    ) -> Dict[str, List[Dict[str, str]]]:
        """
        Retrieve the dimensions for a module or a view
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param view_id: The view_id in a module to retrieve dimensions for
        :return:
            Dict[str, Union[List[Dict[str, str]], str]]: this is the actual
             object that we get back, but the current code does not validate if the value is a list or a string
        """
        # /models/{modelId}/views/{viewId}
        response = self._anaplan_api_call(
            POST_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/views/{view_id}/readRequests/",
            headers={"Content-Type": "application/json"},
            json={"exportType": "TABULAR_SINGLE_COLUMN"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Module dimensions API error",
        )
        return response.json()

    def bulk_pull_request_download(
        self,
        auth_token: str,
        workspace_id: Optional[str],
        model_id: str,
        view_id: str,
        requestId: str,
        pageNo: str
    ) -> Dict[str, List[Dict[str, str]]]:
        """
        Retrieve the dimensions for a module or a view
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param view_id: The view_id in a module to retrieve dimensions for
        :return:
            Dict[str, Union[List[Dict[str, str]], str]]: this is the actual
             object that we get back, but the current code does not validate if the value is a list or a string
        """
        # /models/{modelId}/views/{viewId}
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/views/{view_id}/readRequests/{requestId}/pages/{pageNo}/",
            headers={"Accept": "text/csv"},
            json={},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Module dimensions API error",
        )
        return response

    def bulk_pull_request_clean(
        self,
        auth_token: str,
        workspace_id: Optional[str],
        model_id: str,
        view_id: str,
        requestId: str
    ) -> Dict[str, List[Dict[str, str]]]:
        """
        Retrieve the dimensions for a module or a view
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param view_id: The view_id in a module to retrieve dimensions for
        :return:
            Dict[str, Union[List[Dict[str, str]], str]]: this is the actual
             object that we get back, but the current code does not validate if the value is a list or a string
        """
        # /models/{modelId}/views/{viewId}
        response = self._anaplan_api_call(
            DELETE_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/views/{view_id}/readRequests/{requestId}/",
            headers={"Accept": "application/json"},
            json={},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Module dimensions API error",
        )
        return response.json()

    def bulk_pull_request_status(
        self,
        auth_token: str,
        workspace_id: Optional[str],
        model_id: str,
        view_id: str,
        requestId: str
    ) -> Dict[str, List[Dict[str, str]]]:
        """
        Retrieve the dimensions for a module or a view
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param view_id: The view_id in a module to retrieve dimensions for
        :return:
            Dict[str, Union[List[Dict[str, str]], str]]: this is the actual
             object that we get back, but the current code does not validate if the value is a list or a string
        """
        # /models/{modelId}/views/{viewId}
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/views/{view_id}/readRequests/{requestId}/",
            headers={"Accept": "application/json"},
            json={},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Module dimensions API error",
        )
        return response.json()

    def read_module_data(
        self,
        auth_token: str,
        workspace_id: Optional[str],
        model_id: str,
        line_item_name: Optional[str] = None,
        module_id: Optional[Union[int, str]] = None,
        pages: Optional[List[str]] = None,
    ) -> pd.DataFrame:
        """
        Read the module data according to a line item name or a module ID.
        If reading by line item name: Find the module containing this line item by going over all the
            line items in the model and finding a match. If no match is found raise an exception AnaplanExportError
            If found, export all the data from the module with this line item using the transactional API.
        If reading by module_id, just read the module
        If pages are provided, read the pages in the parameter (see here for value description to the pages:
            https://anaplanbulkapi20.docs.apiary.io/#reference/module-and-view-data
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param line_item_name: The name of the line item (as a string)
        :param module_id: The module ID to read. If not None, we read this module and we don't search for the line item
        :param pages: If not None, this should be a list of pages to read. Pages are defined by dimensionId:itemId
                    e.g. 101000000026:330000000028. See docs for more details: https://anaplanbulkapi20.docs.apiary.io/#reference/module-and-view-data
        :return: a DataFrame with the data from the module
        #todo: adjust to fit a situation where the module doesn't have columns at all(will occur in system modules for example) - sp NO columnCoordinates...

        """
        if module_id is None:
            found_item = self.find_module_by_line_item(auth_token, line_item_name=line_item_name, model_id=model_id)
            module_id = found_item[0]["moduleId"]

        # Now read the data:
        view_res = self.read_transactional_api_view(
            auth_token,
            None,
            model_id,
            module_id,
            accept_type="application/json",
            pages=pages,
        )
        # todo get indicies names
        # Parse it:
        columns = list(chain.from_iterable(view_res["columnCoordinates"]))
        if len(view_res["rows"][0]["rowCoordinates"]) == 1:
            index = [item["rowCoordinates"][0] for item in view_res["rows"]]
        else:
            index = [tuple(item["rowCoordinates"]) for item in view_res["rows"]]

        if columns:
            # in case columns exists
            view_res_df = pd.DataFrame([item["cells"] for item in view_res["rows"]], index=index, columns=columns)
        else:
            # in case columns does npt exists - keep pandas default columns
            view_res_df = pd.DataFrame([item["cells"] for item in view_res["rows"]], index=index)

        return view_res_df

    def add_list_items(
        self,
        auth_token: str,
        workspace_id: str,
        model_id: str,
        list_id: str,
        list_items_df: pd.DataFrame,
    ):
        """
        Add items to an existing list
        :param auth_token: The token
        :param workspace_id: The workspace ID
        :param model_id: The model ID
        :param list_id: The list ID
        :param list_items_df: A DataFrame with at least one column "name" to add. Other possible columns are "code" for
            the item code, "parent" for parent, and other columns with names "p-*" which are properties (e.g. "p-text")
        :return: None if successful, otherwise list of failed entries and their error code
        """
        # /workspaces/{workspaceId}/models/{modelId}/lists/{listId}/items?action=add
        # Create the list items structure according to the doc: https://anaplanbulkapi20.docs.apiary.io/#reference/update-list-items
        items_list = []
        item_cols = list_items_df.columns.intersection(["name", "code", "parent"])
        item_properties = list_items_df.columns[list_items_df.columns.str.startswith("p-")]
        if len(item_properties) == 0:
            item_properties = None
        assert "name" in item_cols, Exception("List Items dataframe MUST include 'name'")

        for _, item_row in list_items_df.iterrows():
            item = {c: item_row[c] for c in item_cols if pd.notnull(item_row[c])}
            if item_properties:
                item["properties"] = {c: item_row[c] for c in item_properties if pd.notnull(item_row[c])}
            items_list.append(item)
        items = {"items": items_list}

        # Now write the request POST:
        response = self._anaplan_api_call(
            POST_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/lists/{list_id}/items/",
            headers={"Accept": "application/json", "Content-Type": "application/json"},
            json=items,
            params={"action": "add"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Write list items API error",
        )

        # If there are any failures, add them to the list:
        resp_json = response.json()
        if resp_json.get("failures"):
            # Failures looks like this:
            # "failures": [
            #         {
            #             "requestIndex": 1,
            #             "failureType": "INCORRECT_FORMAT",
            #             "failureMessageDetails": "incorrect format -- column name:p-number, value:wrong value for number"
            #         }
            #     ]
            # We will keep the original data sent in this index and add it to the failures dict
            failures = resp_json.get("failures")
            for k in range(len(failures)):
                failures[k]["item_data"] = items_list[int(failures[k]["requestIndex"])]
        else:
            failures = None

        return failures

    def list_dimension_items_by_view_id(
        self, auth_token: str, workspace_id: str, model_id: str, view_id: str, dimension_id: str
    ) -> Dict[str, Union[Dict[str, str], List[Dict[str, str]]]]:
        """
        Lists the dimension items by using the view id, rather than model id. This allows us to grab
        the items for time.
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param view_id:
        :param dimension_id:
        :return: response json response
        """
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/views/{view_id}/dimensions/{dimension_id}/items",
            headers={"Accept": "application/json"},
            json={"localeName": "en_US"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Module dimensions API error",
        )
        return response.json()

    def write_cell_data(
        self,
        auth_token: str,
        workspace_id: Optional[str],
        model_id: str,
        module_id: str,
        data_df: pd.DataFrame,
        raise_on_error: bool = True,
    ) -> Optional[List[Dict[str, str]]]:
        """
        Write data to cells in the module. Cells written can only be child cells (no parent can be written as they are
        aggregated).
        :param auth_token: The token
        :param workspace_id: The workspace ID
        :param model_id: The model ID
        :param module_id: The module to write to
        :param data_df: A DataFrame with the data to write. It MUST contain columns with the same names as the dimensions
            in the module (all dimensions MUST be included). Values in the dimension columns MUST be values in those
            dimensions in Anaplan. The other columns in the file MUST be named like line items in the module.
            If the name is not a line item, the column is skipped (or raise an exception if raise_on_error is True)
        :param raise_on_error: Whether to raise an exception on errors, or skip them (if False)
        :return: None if successful, otherwise list of failed entries and their error code
        todo: add a note about transposing time series and pages into columns(header: 'Time'/list names,values: time labels or list items) to update a module with multiple dimensions
        """
        # Get the dimensions and the dimension values in each dimension:
        dimensions = self.get_default_view_dimensions(auth_token, workspace_id, model_id, module_id)
        dim_columns = []
        # Get the list of columns that are the dimensions, and validate all of them are in the dataframe:
        for dim in ["columns", "rows", "pages"]:
            items = dimensions.get(dim)
            if items is not None:
                dim_columns += [col_item["name"] for col_item in items if col_item["name"] != "Line Items"]
        assert len(data_df.columns.intersection(dim_columns)) == len(dim_columns), Exception(
            f"Cant write cell data - missing dimensions {[c for c in dim_columns if c not in data_df.columns]}"
        )

        # Get the line items in the module:
        module_line_items = self.list_line_items(auth_token, workspace_id=None, model_id=model_id, module_id=module_id)
        # Keep only line items that are actually present in the dataframe to write:
        # Todo : str.to_lower to find integraion module line items
        line_item_columns = [item["name"] for item in module_line_items["items"] if item["name"] in data_df.columns]
        if raise_on_error:
            # Make sure we know the line item id for all columns that are not dimensions:
            assert len(data_df.columns.difference(dim_columns + line_item_columns)) == 0, Exception(
                f"Columns that are not line items found! {data_df.columns.difference(dim_columns+line_item_columns)}"
            )

        # Now process the file. We want to chunk it to 1000 cells in each request. We will use the cell_updates
        # list to keep the list of cells we want to write to. When it reaches size == 1000 we write it and continue
        # We go over the rows of the file and identify dimensions and values:
        cell_updates = []
        failures = []

        def _write_to_anaplan(cell_updates: List[Dict[str, str]]):
            # Do the actual write:
            global failures
            response = self._anaplan_api_call(
                POST_REQ,
                endpoint=f"{self.endpoint}/models/{model_id}/modules/{module_id}/data",
                headers={
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                },
                data=json.dumps(cell_updates),
                auth_token=auth_token,
                exception_class=AnaplanExportError,
                exception_message="Write cells API error",
            )

            # If there are any failures, add them to the list:
            resp_json = response.json()
            if resp_json.get("failures"):
                # Failures looks like this:
                # {
                #           "requestIndex": 9,
                #           "failureType": "cellIsInvalid",
                #           "failureMessageDetails": "the cell could not be found or resolved"
                #       }
                # We will keep the original data sent in this index and add it to the failures dict
                failures_to_add = resp_json.get("failures")
                for k in range(len(failures_to_add)):
                    failures_to_add[k]["cell_data"] = cell_updates[int(failures_to_add[k]["requestIndex"])]

        def _add_cell(dims_part: str, line_item_name: str, value: str, cell_updates: List[Dict[str, str]]):
            # Add a line item value cell. If cell_updates is 1000 items long, run the command via the API and flush it
            cell_updates.append(
                {
                    "dimensions": dims_part,
                    "lineItemName": line_item_name,
                    "value": value,
                }
            )
            if len(cell_updates) == CELL_WRITE_CHUNK_SIZE:
                _write_to_anaplan(cell_updates)
                return []
            return cell_updates

        for _, row in data_df.iterrows():
            # Find the dimensions first for this row:
            dims_part = [{"dimensionName": dim_name, "itemName": row[dim_name]} for dim_name in dim_columns]
            # Now add the line items - null or np.nan values are skipped:
            for line_item in line_item_columns:
                if not pd.isnull(row[line_item]):
                    cell_updates = _add_cell(dims_part, line_item, row[line_item], cell_updates)

        # If there are any "left over" cell updates, write them now:
        if len(cell_updates) > 0:
            _write_to_anaplan(cell_updates)

        # Return any failures:
        if len(failures) > 0:
            return failures
        else:
            return None

    def find_module_by_line_item(self, auth_token: str, model_id: str, line_item_name: str) -> List[Dict[str, str]]:
        """
        Find the module containing the line item
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param line_item_name: The name of the line item (as a string)
        :return: A dict with the module found
        """
        line_items_in_model = self.list_line_items(auth_token, None, model_id)
        # found_item = None
        found_items_all: List[Dict[str, str]] = []
        for item in line_items_in_model.get("items", []):
            if line_item_name != None and item["name"].lower() == line_item_name.lower():
                # We found the line item!
                # found_item = item
                found_items_all.append(item)
                # break
        if found_items_all is None:
            # Line item not found. Raise an exception:
            raise AnaplanExportError(f"Line item {line_item_name} not found in model")
        return found_items_all

    def get_model_details(self, auth_token: str, model_id: str) -> Dict[str, Any]:
        """
        Return the model details by model ID (https://anaplanbulkapi20.docs.apiary.io/#reference/models)
        :param auth_token: The auth token
        :param model_id: The model ID to query
        :return: The dict with the JSON response
        """
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/models/{model_id}",
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Get model details API error",
        )
        return response.json()

    def list_views(self, auth_token: str, workspace_id: str, model_id: str):
        """
        List views
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :return:
        """
        return self.__list_objs(auth_token, None, model_id, "views")

    def list_files(self, auth_token: str, workspace_id: str, model_id: str):
        """
        List files
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :return:
        """
        return self.__list_objs(auth_token, workspace_id, model_id, "files")

    def list_imports(self, auth_token: str, workspace_id: str, model_id: str):
        """
        List imports
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :return:
        """
        return self.__list_objs(auth_token, workspace_id, model_id, "imports")

    def list_exports(self, auth_token: str, workspace_id: str, model_id: str):
        """
        List exports
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :return:
        """
        return self.__list_objs(auth_token, workspace_id, model_id, "exports")

    def list_modules(self, auth_token: str, workspace_id: str, model_id: str):
        """
        List modules
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :return: A JSON describing the modules in the model
        """
        return self.__list_objs(auth_token, None, model_id, "modules")

    def list_line_items(
        self, auth_token: str, workspace_id: Optional[str], model_id: str, module_id: Union[int, str, None] = None
    ):
        """
        List Line items in the model
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param module_id: If not None, list line items only in this module
        :return: A JSON describing the line items in the model
        """
        return self.__list_objs(auth_token, None, model_id, "lineItems", module_id=module_id)

    def list_lists(self, auth_token: str, workspace_id: str, model_id: str):
        """
        List all lists in the model
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :return: A JSON describing the line items in the model
        """
        return self.__list_objs(auth_token, workspace_id, model_id, "lists")

    def list_models(self, auth_token: str, workspace_id=None):
        """
        List models visible to the user
        :param auth_token: The auth token string
        :return: A JSON describing models
        """
        return self.__list_objs(auth_token, workspace_id, None, "models")

    def list_items_in_dimension(self, auth_token: str, workspace_id: str, model_id: str, dimension_id: str):
        """
        List the items in a dimension
        IMPORTANT: Currently the dimension must be a list, a list subset, a line item subset,
        or the Users dimension -NO TIME OR LINE ITEM DIMENSIONS ARE SUPPORTED
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param dimension_id: The ID of the dimension
        :return:
        """
        endpoint = f"{self.endpoint}/models/{model_id}/dimensions/{dimension_id}/items"
        headers = {
            "Content-Type": "application/json",
            "Authorization": "AnaplanAuthToken " + auth_token,
        }

        response = self._get(endpoint, headers=headers)
        if not response.ok:
            raise AnaplanAPIError(f"failed to list items for dimension. error: {response.content}")

        # Extract the data from the response:
        return response.json()

    def get_view_dimensions(
        self,
        auth_token: str,
        workspace_id: Optional[str],
        model_id: str,
        module_id: str,
    ):
        """
        Retrieve the dimensions for a module or a view
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param module_id: The module_id or view_id in a module to retrieve dimensions for
        :return:
        """
        # /models/{modelId}/views/{viewId}
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/models/{model_id}/views/{module_id}/",
            headers={"Accept": "application/json"},
            json={"localeName": "en_US"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Module dimensions API error",
        )
        return response.json()

    def __list_objs(
        self,
        auth_token: str,
        workspace_id: Union[str, None],
        model_id: str,
        obj: str = "files",
        module_id: Union[int, str, None] = None,
    ):
        """
        Returns a list of the objects - obj can be 'files' or 'imports'
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param obj:
        :return: response json content.
        """
        endpoint = self.endpoint
        if workspace_id is not None:
            endpoint += f"/workspaces/{workspace_id}"
        if model_id is not None:
            endpoint += f"/models/{model_id}"
        if module_id is not None:
            endpoint += f"/modules/{module_id}"
        endpoint += f"/{obj}/"
        headers = {
            "Content-Type": "application/json",
            "Authorization": "AnaplanAuthToken " + auth_token,
        }

        response = self._get(endpoint, headers=headers)
        if not response.ok:
            raise AnaplanAPIError(f"failed to list objects of type: {obj}. error: {response.content}")

        # Extract the data from the response:
        return response.json()

    def get_model_current_period(self, auth_token: str, workspace_id: str, model_id: str) -> Dict[str, Any]:
        """
        Retrieve the current period from the model's calendar
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :return: A JSON describing the calendar items in the model
        """
        meta_data = self.__list_objs(auth_token, None, model_id, "currentPeriod")
        current_period = meta_data["currentPeriod"]["periodText"]
        return current_period

    def get_model_current_period_last_day(self, auth_token: str, workspace_id: str, model_id: str) -> Dict[str, Any]:
        """
        Retrieve the current period from the model's calendar
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :return: A JSON describing the calendar items in the model
        """
        meta_data = self.__list_objs(auth_token, None, model_id, "currentPeriod")
        current_period_last_day = meta_data["currentPeriod"]["lastDay"]
        return current_period_last_day

    def get_model_calendar_type(self, auth_token: str, workspace_id: str, model_id: str) -> Dict[str, Any]:
        """
        Retrieve the current period from the model's calendar
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :return: A JSON describing the calendar items in the model
        """
        meta_data = self.__list_objs(auth_token, None, model_id, "currentPeriod")
        current_period_calendar_type = meta_data["currentPeriod"]["calendarType"]
        return current_period_calendar_type

    def get_model_fiscal_year(self, auth_token: str, workspace_id: str, model_id: str) -> Dict[str, Any]:
        """
        Retrieve the fiscal year from the model's calendar
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :return: A JSON describing the calendar items in the model
        """
        meta = self.__list_objs(auth_token, None, model_id, "modelCalendar")
        fiscal_year = meta["modelCalendar"]["fiscalYear"]["year"]
        return fiscal_year

    def get_model_fiscal_year_startdate(self, auth_token: str, workspace_id: str, model_id: str) -> Dict[str, Any]:
        """
        Retrieve the fiscal year from the model's calendar
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :return: A JSON describing the calendar items in the model
        """
        meta = self.__list_objs(auth_token, None, model_id, "modelCalendar")
        fiscal_year_start_date = meta["modelCalendar"]["fiscalYear"]["startDate"]
        return fiscal_year_start_date

    def get_model_fiscal_year_enddate(self, auth_token: str, workspace_id: str, model_id: str) -> Dict[str, Any]:
        """
        Retrieve the fiscal year from the model's calendar
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :return: A JSON describing the calendar items in the model
        """
        meta = self.__list_objs(auth_token, None, model_id, "modelCalendar")
        fiscal_year_end_date = meta["modelCalendar"]["fiscalYear"]["endDate"]
        return fiscal_year_end_date

    def delete_list_items(
        self,
        auth_token: str,
        workspace_id: str,
        model_id: str,
        list_id: str,
        item_ids: List[str],
    ) -> Optional[List[Dict[str, str]]]:
        """
        Delete items from an existing list
        :param auth_token: The token
        :param workspace_id: The workspace ID
        :param model_id: The model ID
        :param list_id: The list ID
        :param item_ids: List of item ids as str todo: add ability to use code instead of id's if needed (currently API can't use names...)
        :return: None if successful, otherwise list of failed entries and their error code
        """
        # create items  as json

        items: Dict[str, List[Dict[str, str]]] = {"items": []}

        for item_id in item_ids:
            items["items"].append({"id": item_id})

            # Now write the request POST:
        response = self._anaplan_api_call(
            POST_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/lists/{list_id}/items?delete=true",
            headers={
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            json=items,
            params={"action": "delete"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Delete list items API error",
        )

        # If there are any failures, add them to the list:
        resp_json = response.json()
        if resp_json.get("failures"):
            # Failures looks like this:
            # "failures": [
            #         {
            #             "requestIndex": 1,
            #             "failureType": "INCORRECT_FORMAT",
            #             "failureMessageDetails": "incorrect format -- column name:p-number, value:wrong value for number"
            #         }
            #     ]
            # We will keep the original data sent in this index and add it to the failures dict
            failures = resp_json.get("failures")
            for k in range(len(failures)):
                failures[k]["item_data"] = items["items"][int(failures[k]["requestIndex"])]
        else:
            failures = None

        return failures

    def get_all_line_items_metadata_by_module(
        self, auth_token: str, workspace_id: str, model_id: str, module_id: str, include_all: bool = True
    ) -> Dict[str, str]:
        """
        Get all the line item metadata for a module including formula, datatype, formats, time ranges, etc.
        Note:This is an early access endpoint and is not generally available yet
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param module_id:
        :param include_all: if False return only name and id for the line items
        :return: response json response
        """
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/modules/{module_id}/lineItems",
            headers={"Accept": "application/json"},
            params={"includeAll": "true" if include_all else "false"},
            json={"localeName": "en_US"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Module dimensions API error",
        )
        return response.json()

    def get_list_items(
        self,
        auth_token: str,
        workspace_id: Optional[str],
        model_id: str,
        list_id: str,
        include_all: bool = False,
        include_hierarchy: bool = True,
    ) -> Dict[str, List[Dict[str, str]]]:
        """
        Get the items in a list
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param list_id:
        :param include_all: If True, we include all data including custom properties for each list item
        :param include_hierarchy: If True, we include parent members in the hierarchy with 2 more fields for their list details,
        :return:

        """
        # /workspaces/{workspaceId}/models/{modelId}/lists/{listId}/items
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/lists/{list_id}/items",
            headers={"Accept": "application/json"},
            params={"includeAll": "true" if include_all else "false"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="List items API error",
        )
        if not include_hierarchy and response.json()["listItems"]:  # checks that the list is not empty
            new_response = self._remove_outside_members_from_list(response.json())
            return new_response
        else:
            return response.json()

    def get_list_metadata(
        self,
        auth_token: str,
        workspace_id: Optional[str],
        model_id: str,
        list_id: str,
    ) -> Dict[str, List[Dict[str, str]]]:
        """
        Get the metadata about a list including its parent, whether it has selective access, its top level item etc
        :param auth_token:
        :param workspace_id:
        :param model_id:
        :param list_id:
        :return:

        """
        # /workspaces/{workspaceId}/models/{modelId}/lists/{listId}
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/workspaces/{workspace_id}/models/{model_id}/lists/{list_id}",
            headers={"Accept": "application/json"},
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="List items API error",
        )
        try:
            return response.json()["metadata"]
        except JSONDecodeError as e:
            raise AnaplanAPIError(
                f"Error getting list metadata for {workspace_id}/models/{model_id}/lists/{list_id}: response {response} is not json"
            ) from e
        except KeyError as e:
            raise AnaplanAPIError(
                f"Error getting list metadata for {workspace_id}/models/{model_id}/lists/{list_id}: response {json} has no metadata"
            ) from e

    def list_views_for_module(
        self, auth_token: str, workspace_id: str, model_id: str, module_id: str
    ) -> Dict[str, str]:
        """
        List views for a specific module
        :param auth_token: The auth token
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID to query
        :param module_id: The module ID to query
        :return:
        """
        response = self._anaplan_api_call(
            GET_REQ,
            endpoint=f"{self.endpoint}/models/{model_id}/modules/{module_id}/views",
            auth_token=auth_token,
            exception_class=AnaplanExportError,
            exception_message="Get views for specific module API error",
        )
        return response.json()

    def list_workspaces(self, auth_token: str, tenantDetails: bool = True) -> Dict[str, str]:
        """
        List workspaces associated with a tenant (according to the token)
        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID
        :param tenantDetails: When set to true, this call returns an estimate of the current size of the workspace from all the models it contains. When set to false, or not defined, this call does not return an estimated current size of the workspace
        :return: A JSON describing the modules in the model
        """
        endpoint = f"{self.endpoint}/workspaces?tenantDetails={'true' if tenantDetails else 'false'}"
        headers = {
            "Content-Type": "application/json",
            "Authorization": "AnaplanAuthToken " + auth_token,
        }
        response = self._get(endpoint, headers=headers)

        if not response.ok:
            raise AnaplanAPIError(f"failed to list workspaces. error: {response.content}")
        return response.json()

    def _remove_outside_members_from_list(self, items_obj: Dict[str, Dict[str, str]]) -> Dict[str, Dict[str, str]]:
        """
        Input the json response object from get_list_items including hierarchies and returns it clean
        :param items_obj: object from get_list_items call
        :return: same object without the hierarichal  members
        """
        df = pd.json_normalize(items_obj["listItems"])
        if "parent" in df.columns:

            mask = ~df["name"].isin(
                df["parent"].to_list()
            )  # removing all items that exist in the parent columns while keeping only the lowest level in hierarchy
            df = df[mask]
        items_obj["listItems"] = df.to_dict("records")
        return items_obj

    def get_all_lists_hierarchies(self, auth_token: str, workspace_id: str, model_id: str) -> "pd.DataFrame":
        """
        Retrieve a dataframe containing all lists in module along with their hierarchies
        rows with null in the id columns meaning it's a divider and contain no items..

        :param auth_token: The auth token string
        :param workspace_id: None. Not needed in this call (kept for compatibility with other calls in the class)
        :param model_id: The model ID

        :return: Dataframe containing all lists hierarchies

        todo: add validation for list sizes limits according to anplans api limitations
        """
        all_lists = self.list_lists(auth_token, workspace_id, model_id)
        df_all_lists = pd.DataFrame(all_lists["lists"])
        df_all_items = pd.DataFrame()
        for list_id in df_all_lists["id"]:
            list_items_response = self.get_list_items(
                auth_token, workspace_id, model_id, list_id, include_hierarchy=False
            )
            listItems = list_items_response["listItems"]
            if not listItems:
                df_items = pd.DataFrame(
                    columns=[
                        "id",
                        "name",
                        "code",
                        "parent",
                        "parentId",
                        "listId",
                        "listName",
                    ]
                )
                df_items.at[0, "listId"] = list_id
                list_name = df_all_lists["name"][df_all_lists["id"] == list_id].to_list()
                df_items.at[0, "listName"] = list_name[0]
            else:
                # listItems_clean = self._remove_outside_members_from_list(listItems)
                # df_items = pd.DataFrame(listItems_clean)

                df_items = pd.DataFrame(listItems)
                df_items["listId"] = list_id
                list_name = df_all_lists["name"][df_all_lists["id"] == list_id].to_list()
                df_items["listName"] = list_name[0]
            df_all_items = df_all_items.append(df_items, ignore_index=True)
        return df_all_items

    def _extract_lists_ids_and_names(self, lists_obj):
        list_ids_names = {}

        for list_index in range(0, len(lists_obj["lists"])):
            list_ids_names[lists_obj["lists"][list_index]["id"]] = lists_obj["lists"][list_index]["name"]
        return list_ids_names

    def _find_list(
        self, token_val: str, workspace_id: str, model_id: str, lists_ids_and_names_dict: Dict[str, str], item_name: str
    ) -> List[Dict[str, str]]:
        """
        Inner method for lookup_list_by_itme_name, that takes dictionary containing list ids and names and
        check to see if one of the names matches what we are looking for in item_name

        :param lists_ids_and_names_dict: dictionary of ids and names for all
        :param item_name: desired name to match
        :return: list of anaplan lists that contained the name we look for
        """
        matched_lists = []
        for listid in lists_ids_and_names_dict.keys():
            items = self.get_list_items(token_val, workspace_id, model_id, listid, include_hierarchy=False)

            for index in range(0, len(items["listItems"])):
                if items["listItems"][index]["name"].lower() == item_name.lower():
                    matched_lists.append({listid: lists_ids_and_names_dict[listid]})

        return matched_lists

    def lookup_list_by_item_name(
        self, auth_token: str, workspace_id: str, model_id: str, item_name: str
    ) -> List[Dict[str, str]]:
        """
        Return the corresponding list as dict of list_id:list_name to any string input as list item name
        if the item name exit in more than a single list it will return them all
        comparison is in lower case so case incensitive
        :param auth_token: The auth token string
        :param workspace_id: The workspace id
        :param model_id: The model ID
        :param item_name : the item we are looking (str) case insensitive
        :return: list
        todo: Future add ability to search by the display name in cases it's a numbered list like #E2 Employees..use include_all=True in get_list_items
        """
        all_lists = self.list_lists(auth_token, workspace_id, model_id)
        all_lists_ids_and_names = self._extract_lists_ids_and_names(all_lists)
        lists_id_and_name = self._find_list(auth_token, workspace_id, model_id, all_lists_ids_and_names, item_name)
        return lists_id_and_name

    def lookup_line_item_by_name(
        self, auth_token: str, workspace_id: str, model_id: str, item_name: str
    ) -> List[Dict[str, str]]:
        """
        Extracts the matched line items to the item_name from an object containing all line items and return a list of objects that looks like this:
         [{'moduleId': '102000000024', 'moduleName': 'DATA01 P&L Actuals & Budget', 'id': '268000000001', 'name': 'Revenue'},
         {'moduleId': '102000000025', 'moduleName': 'DATA02 P&L NET', 'id': '268000000005', 'name': 'Revenue'},...]
        comparison case-incensitive
        :param auth_token: The auth token string
        :param workspace_id: The workspace id
        :param model_id: The model ID
        :param item_name : the item we are looking (str)
        :return: list of matcehd object
        """
        all_line_items = self.list_line_items(auth_token, workspace_id, model_id)

        line_items_in_modules = []
        for index in range(0, len(all_line_items["items"])):
            if all_line_items["items"][index]["name"].lower() == item_name.lower():
                line_items_in_modules.append(all_line_items["items"][index])
        return line_items_in_modules

    def _retrieve_relevant_lists(self, all_dimensions: dict) -> List[Dict[str, str]]:
        """
        Inner function to retrieve the lists dimension from all dimensions in module
        :param all_dimensions: object containing all dimension as recieved from get_view
        :return: lists names and ids that exist in module as dimension
        """
        relevant_lists = []
        for dimension in ["columns", "rows", "pages"]:
            if dimension not in all_dimensions.keys():
                continue
            dimension_len = len(all_dimensions[dimension])
            for i in range(0, dimension_len):
                if (
                    all_dimensions[dimension][i]["name"] == "Time"
                    or all_dimensions[dimension][i]["name"] == "Line Items"
                ):
                    continue
                relevant_lists.append(all_dimensions[dimension][i])
        return relevant_lists

    def _assign_headers(self, df: "pd.DataFrame", dimensions: Dict[str, List[Dict[str, str]]]) -> "pd.DataFrame":
        """
        Method to rename column headers to the name of the list
        :param df: df containg the read_module_data with no index
        :param dimensions: dictionary object cantaning dimensiions for moudle from get_default_view_dimensions
        :return: df adjusted with new headers
        """
        relevant_columns = [col for col in df.columns if "level_" in col]

        for dims_index, column in enumerate(relevant_columns):
            df.rename(columns={column: dimensions["rows"][dims_index]["name"]}, inplace=True)
        return df

    def adjust_time_series_module_read(
        self, df: pd.DataFrame, all_dimensions: Dict[str, List[Dict[str, str]]]
    ) -> "pd.DataFrame":
        """
        Method will take a module consisting of complexed string as index+ time in columns and will
        return turning the index into columns
        todo: method intended to work on view that has TIME dimension in columns and the rest is rows (time series..)

        :param df: pd.DataFrame containing the read_module_data output
        :param dimensions : dictionary object containing dimensions for module from get_default_view_dimensions
        :return: pd.DataFrame adjusted shape with headers populated
        """
        df.index = pd.MultiIndex.from_tuples(df.index)
        df.reset_index(inplace=True)
        return self._assign_headers(df, all_dimensions)

    def remove_summation_lines_time_series_module(
        self,
        token_val: str,
        workspace_id: str,
        model_id: str,
        df_module_data: pd.DataFrame,
        all_dimensions: Dict[str, List[Dict[str, str]]],
    ) -> "pd.DataFrame":
        """
        Method will take as input the data from module after it was reformatted as columns and return it containing only the lowest level of the hierarchy
        :param df_module_data: dataframe that is shaped with all dimensions in columns
        :param all_dimensions : dictionary object containing dimensions for module from get_default_view_dimensions
        :return: dataframe of the same shape as input without upper level hierarchy items
        """
        relevant_lists = self._retrieve_relevant_lists(all_dimensions)

        for index in range(0, len(relevant_lists)):
            list_name = relevant_lists[index]["name"]
            list_items = self.get_list_items(
                token_val, workspace_id, model_id, relevant_lists[index]["id"], include_hierarchy=False
            )
            temp_df = pd.DataFrame(list_items["listItems"])
            mask = df_module_data[list_name].isin(temp_df["name"].to_list())
            df_module_data = df_module_data[mask]
        return df_module_data, relevant_lists


#### for debugging######################################################
if __name__ == "__main__":
    import json

    from anaplan_dal.anaplan_auth import AnaplanAuthClient

    auth_url = "https://auth.anaplan.com"  # PRODUCTION
    api_url = "https://api.anaplan.com/2/0/"  # PRODUCTION
    # auth_url = "https://us1a.app-r2p2.anaplan.com"  # NON PROD
    # api_url = "https://us1a.app-r2p2.anaplan.com/2/0"  # NON PROD
    creds_dict = json.load(
        open(
            "/Users/ofir/Documents/05-25-21 - Data PreProcessing/creds ofir new.json",
            "rb",
        )
    )

    username = creds_dict["username"]
    password = creds_dict["password"]

    # Authenticate with Anaplan:
    auth_client = AnaplanAuthClient(base_endpoint=auth_url)
    token = auth_client.authenticate(username, password)
    token_val = token["tokenValue"]

    act_client = AnaplanActionsClient(endpoint=api_url, save_responses=True)

    WORKSPACE_ID = "2c9ba1b67cc7db4c017d4beadeda5c8b"  # NON PROD
    MODEL_ID = "8D18A07E78954A04B6DC95C1836DB5B2"  # NON PROD sales analysis
    MODULE_ID = 102000000000  # NON PROD

    workspace_id = "8a81b0126ee24b88016fa8f592ab3df7"
    model_id = "CE7A8EDE5CCC46DE976E6DA3549F056D"  # L1MB - Unitests
    module_id = "102000000035"
    list_id = "101000000003"
    export_id = "116000000001"
    item_name_lineitem = "revenue"
    item_name_list = "london"

    # all_dims = act_client.get_default_view_dimensions(token_val, workspace_id, model_id, module_id)

    response1 = act_client.get_list_upper_hierarchies(token_val, workspace_id, model_id, list_id)
    print(response1)

    response11 = act_client.get_list_items(token_val, workspace_id, model_id, list_id, include_hierarchy=False)

    workspace_id = "8a81b0126ee24b88016fa8f592ab3df7"
    model_id = "59F7FE51B703468D948795CC39532653"  # sales analysis
    module_id = "102000000001"
    list_id2 = "101000000001"
    response2 = act_client.get_list_items(token_val, workspace_id, model_id, list_id2)

    response22 = act_client.get_list_items(token_val, workspace_id, model_id, list_id2, include_hierarchy=False)

    list_id3 = "101000000003"
    response3 = act_client.get_list_items(token_val, workspace_id, model_id, list_id3)
    response33 = act_client.get_list_items(token_val, workspace_id, model_id, list_id3, include_hierarchy=False)

    response = act_client.get_list_items(token_val, workspace_id, model_id)
    # pd.DataFrame(response2['listItems'])
    all_dims = act_client.get_default_view_dimensions(token_val, workspace_id, model_id, module_id)

    response_original = act_client.read_module_data(token_val, workspace_id, model_id, None, module_id)
    new_response = act_client.adjust_time_series_module_read(response_original, all_dims)
    clean_response_no_totals, relevant_lists = act_client.remove_summation_lines_time_series_module(
        token_val, workspace_id, model_id, new_response, all_dims
    )
    print(clean_response_no_totals)
